package com.sprintgether.istudentfy.controller;

import com.sprintgether.istudentfy.model.Student;
import com.sprintgether.istudentfy.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private StudentRepository studentRepository;

    @GetMapping("/all")
    public List<Student> getStudents(){
        return studentRepository.findAll();
    }
}
